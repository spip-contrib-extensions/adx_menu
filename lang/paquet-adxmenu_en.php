<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-adxmenu
// Langue: fr
// Date: 07-05-2012 21:47:08
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'adxmenu_description' => '{{Multilevel opening menu type \'fly-out\' in CSS/Javacript}}

The plugin adds the new tag <code>#ADXMENU{options}</code> and the "{{adxmenu.html}}" model for your SPIP skeletons, which brings up an elegant menu type \'{fly-out}\'. The generated menu style is customizable using CSS through class "{adxm adxmenu}" usable outside the model.

An internal documentation page is available if the plugin is enabled : [->../?page=adxmenu_documentation].',
	'adxmenu_slogan' => 'Multilevel opening menu type \'fly-out\' in CSS/Javacript',
	'adxmenu_nom' => 'ADX Menu',
);
?>