<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-adxmenu
// Langue: fr
// Date: 07-05-2012 21:47:08
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A
	'adxmenu_description' => '{{Menu ouvrant multi-niveaux type \'fly-out\' en CSS/javascript}}

Ce plugin ajoute la balise <code>#ADXMENU{options}</code> et le modèle "{{adxmenu.html}}" pour vos squelettes SPIP, qui fait apparaître un menu élégant type \'{fly-out}\'. Le style du menu généré est personnalisable par CSS, via la classe "{adxm adxmenu}" utilisable en dehors du modèle.

Une documentation interne est disponible lorsque le plugin est actif sur la page publique [adxmenu_documentation->../?page=adxmenu_documentation].',
	'adxmenu_slogan' => 'Menu ouvrant multi-niveaux type \'fly-out\' en CSS/javascript',
	'adxmenu_nom' => 'ADX Menu',
);
?>